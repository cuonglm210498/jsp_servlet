use shop_clothes_05;

CREATE TABLE `user` (
  `id` int PRIMARY KEY AUTO_INCREMENT,
  `username` varchar(50) unique not null,
  `password` varchar(50) unique not null,
  `name` varchar(50),
  `address` varchar(50),
  `phone` varchar(20),
  `email` varchar(50),
  `birthday` timestamp,
  `created_by` varchar(50),
  `created_date` timestamp default CURRENT_TIMESTAMP
);

CREATE TABLE `role` (
  `id` int PRIMARY KEY not null AUTO_INCREMENT,
  `name` varchar(255),
  `description` varchar(255)
);

CREATE TABLE `permission` (
  `user_id` int,
  `role_id` int,
  primary key(user_id, role_id)
);

CREATE TABLE `category` (
  `id` int PRIMARY KEY AUTO_INCREMENT,
  `name` varchar(255)
);

CREATE TABLE `sub_category` (
  `id` int PRIMARY KEY AUTO_INCREMENT,
  `name` varchar(255),
  `category_id` int
);

CREATE TABLE `color` (
  `id` int PRIMARY KEY AUTO_INCREMENT,
  `name` varchar(255)
);

CREATE TABLE `size` (
  `id` int PRIMARY KEY AUTO_INCREMENT,
  `name` varchar(255)
);

CREATE TABLE `form` (
  `id` int PRIMARY KEY AUTO_INCREMENT,
  `name` varchar(255)
);

CREATE TABLE `collar_stype` (
  `id` int PRIMARY KEY AUTO_INCREMENT,
  `name` varchar(255)
);

CREATE TABLE `material` (
  `id` int PRIMARY KEY AUTO_INCREMENT,
  `name` varchar(255)
);

CREATE TABLE `steeve_stype` (
  `id` int PRIMARY KEY AUTO_INCREMENT,
  `name` varchar(255)
);

CREATE TABLE `vignette` (
  `id` int PRIMARY KEY AUTO_INCREMENT,
  `name` varchar(255)
);

CREATE TABLE `species` (
  `id` int PRIMARY KEY AUTO_INCREMENT,
  `name` varchar(255)
);

CREATE TABLE `trourse_stype` (
  `id` int PRIMARY KEY AUTO_INCREMENT,
  `name` varchar(255)
);

CREATE TABLE `product` (
  `id` int PRIMARY KEY AUTO_INCREMENT,
  `name` varchar(255),
  `description` text,
  `thumbnail` varchar(255),
  `album` text,
  `price` long
);

CREATE TABLE `product_item` (
  `id` int PRIMARY KEY AUTO_INCREMENT,
  `product_id` int not null,
  `size_id` int not null,
  `color_id` int not null,
  `form_id` int null,
  `collar_stype_id` int null,
  `material_id` int null,
  `steeve_stype_id` int null,
  `vignette_id` int null,
  `species_id` int null,
  `trourse_stype_id` int null,
  `number` int not null,
  `price` int,
  `created_date` timestamp default current_timestamp,
  `created_by` varchar(50)
);

CREATE TABLE `blog` (
  `id` int PRIMARY KEY AUTO_INCREMENT,
  `title` varchar(255),
  `description` varchar(250),
  `content` longtext,
  `thumbnail` varchar(255),
  `created_by` varchar(255),
  `created_date` timestamp
);

CREATE TABLE `tag` (
  `id` int PRIMARY KEY AUTO_INCREMENT,
  `name` varchar(255)
);

CREATE TABLE `blog_tag` (
  `blog_id` int,
  `tag_id` int
);

ALTER TABLE `permission` ADD FOREIGN KEY (`user_id`) REFERENCES `user` (`id`);

ALTER TABLE `permission` ADD FOREIGN KEY (`role_id`) REFERENCES `role` (`id`);

ALTER TABLE `sub_category` ADD FOREIGN KEY (`category_id`) REFERENCES `category` (`id`);

ALTER TABLE `product_item` ADD FOREIGN KEY (`product_id`) REFERENCES `category` (`id`);

ALTER TABLE `product_item` ADD FOREIGN KEY (`size_id`) REFERENCES `size` (`id`);

ALTER TABLE `product_item` ADD FOREIGN KEY (`color_id`) REFERENCES `color` (`id`);

ALTER TABLE `product_item` ADD FOREIGN KEY (`form_id`) REFERENCES `form` (`id`);

ALTER TABLE `product_item` ADD FOREIGN KEY (`collar_stype_id`) REFERENCES `collar_stype` (`id`);

ALTER TABLE `product_item` ADD FOREIGN KEY (`material_id`) REFERENCES `material` (`id`);

ALTER TABLE `product_item` ADD FOREIGN KEY (`steeve_stype_id`) REFERENCES `steeve_stype` (`id`);

ALTER TABLE `product_item` ADD FOREIGN KEY (`species_id`) REFERENCES `species` (`id`);

ALTER TABLE `product_item` ADD FOREIGN KEY (`trourse_stype_id`) REFERENCES `trourse_stype` (`id`);

ALTER TABLE `blog_tag` ADD FOREIGN KEY (`blog_id`) REFERENCES `blog` (`id`);

ALTER TABLE `blog_tag` ADD FOREIGN KEY (`tag_id`) REFERENCES `tag` (`id`);

select * from product_item;
