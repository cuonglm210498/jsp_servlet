<%--
  Created by IntelliJ IDEA.
  User: lemanhcuong
  Date: 02/10/2020
  Time: 10:28 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@include file="/common/taglib.jsp"%>
<!doctype html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta name="description" content="Crush On The most popular Admin Dashboard template and ui kit">
    <meta name="author" content="PuffinTheme the theme designer">

    <link rel="icon" href="favicon.ico" type="image/x-icon" />

    <title>Admin</title>

    <!-- Bootstrap Core and vandor -->
    <link rel="stylesheet" href="<c:url value="/assets/plugins/bootstrap/css/bootstrap.min.css" />" />

    <!-- Core css -->
    <link rel="stylesheet" href="<c:url value="/assets/css/main.css" />" />
    <link rel="stylesheet" href="<c:url value="/assets/css/default.css" />" />
</head>

<body class="font-opensans">
    <!-- Page Loader -->
    <div class="page-loader-wrapper">
        <div class="loader">
        </div>
    </div>
    <!-- Overlay For Sidebars -->

    <div id="main_content">

        <div id="header_top" class="header_top">
            <div class="container">
                <div class="hleft">
                    <a class="header-brand" href="#"><i class="fa fa-dashboard brand-logo"></i></a>
                </div>
            </div>
        </div>

        <div id="rightsidebar" class="right_sidebar"></div>

        <div class="theme_div">
            <div class="card">
                <div class="card-body"></div>
            </div>
        </div>
        <div id="left-sidebar" class="sidebar ">
            <h5 class="brand-name">Admin <a href="javascript:void(0)" class="menu_option float-right"><i class="icon-grid font-16" data-toggle="tooltip" data-placement="left" title="Grid & List Toggle"></i></a></h5>
            <nav id="left-sidebar-nav" class="sidebar-nav">
                <ul class="metismenu">
                    <li class="g_heading">Directories</li>
                    <li><a href="#"><i class="fa fa-user"></i><span>Quản lý user</span></a></li>

                </ul>
            </nav>
        </div>

        <div class="page">
            <div id="page_top" class="section-body">
                <div class="container-fluid">
                    <div class="page-header">
                        <div class="left">
                            <h1 class="page-title">Search</h1>
                            <div class="input-group xs-hide">
                                <input type="text" class="form-control" placeholder="Search...">
                            </div>
                        </div>
                        <div class="right">
                            <ul class="nav nav-pills">
                                <li class="nav-item dropdown"></li>
                                <li class="nav-item dropdown"></li>
                                <li class="nav-item dropdown"></li>
                            </ul>
                            <div class="notification d-flex">
                                <div class="dropdown d-flex">
                                    <a class="nav-link icon d-none d-md-flex btn btn-default btn-icon ml-1" data-toggle="dropdown"><i class="fa fa-user"></i></a>
                                    <div class="dropdown-menu dropdown-menu-right dropdown-menu-arrow">
                                        <a class="dropdown-item" href="page-profile.html"><i class="dropdown-icon fe fe-user"></i> Profile</a>
                                        <a class="dropdown-item" href="login.html"><i class="dropdown-icon fe fe-log-out"></i> Sign out</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="section-body">
                <div class="container-fluid">
                    <div class="col-lg-12">
                        <div class="card bg-none">
                            <div class="card-header">
                                <h3 class="card-title">Users</h3>
                            </div>
                            <div class="card-body pt-0">
                                <div class="table-responsive table_e2">
                                    <table class="table table-hover table-vcenter table_custom spacing5 text-nowrap mb-0">
                                        <thead>
                                        <tr>
                                            <th>
                                                <label class="custom-control custom-checkbox mb-0">
                                                    <input type="checkbox" class="custom-control-input" name="example-checkbox1" value="option1" checked>
                                                    <span class="custom-control-label">&nbsp;</span>
                                                </label>
                                            </th>
                                            <th>Name</th>
                                            <th>Phone</th>
                                            <th>Address</th>
                                            <th>Action</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                            <c:forEach items="${users.data}" var="user" >
                                                <tr>
                                                    <td class="width45">
                                                        <label class="custom-control custom-checkbox mb-0">
                                                            <input type="checkbox" class="custom-control-input" name="example-checkbox1" value="option1" checked>
                                                            <span class="custom-control-label">&nbsp;</span>
                                                        </label>
                                                    </td>
                                                    <td>
                                                        <img src="<c:url value="/assets/images/xs/avatar1.jpg" />" class="rounded" alt="" />
                                                        <span class="c_name">${user.getUserName()}</span>
                                                    </td>
                                                    <td>
                                                        <span class="phone"><i class="fa fa-phone mr-2"></i>${user.getPhone()}</span>
                                                    </td>
                                                    <td>
                                                        <span><i class="fa fa-map-marker"></i>${user.getAddress()}</span>
                                                    </td>
                                                    <td>
                                                        <button type="button" class="btn btn-primary btn-sm" title="Edit"><i class="fa fa-edit"></i></button>
                                                        <button type="button" class="btn btn-danger btn-sm" title="Delete"><i class="fa fa-trash-o"></i></button>
                                                    </td>
                                                </tr>
                                            </c:forEach>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script src="<c:url value="/assets/bundles/lib.vendor.bundle.js" />"></script>

    <script src="<c:url value="/assets/js/core.js" />"></script>
</body>

</html>
