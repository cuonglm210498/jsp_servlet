package com.lecuong.mapper;

import com.lecuong.entity.Role;
import com.lecuong.model.response.role.RoleResponse;
import com.lecuong.util.ReflectionUtil;

import java.lang.reflect.InvocationTargetException;

public class RoleMapper {

    public static RoleResponse convertToResponse(Role role) {

        RoleResponse roleResponse = new RoleResponse();
        try {
            ReflectionUtil.mapper(role, roleResponse);
        } catch (NoSuchMethodException | IllegalAccessException | InvocationTargetException e) {
            e.printStackTrace();
        }

        return roleResponse;
    }

}
