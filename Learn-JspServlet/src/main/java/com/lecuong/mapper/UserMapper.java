package com.lecuong.mapper;

import com.lecuong.annotation.Component;
import com.lecuong.entity.User;
import com.lecuong.model.response.user.UserResponse;

import java.lang.reflect.InvocationTargetException;

import static com.lecuong.util.ReflectionUtil.*;
import static com.lecuong.util.TimeUtil.*;

public class UserMapper {

    public static UserResponse mapToResponse(User user)  {
        UserResponse userResponse = new UserResponse();
        try {
            mapper(user, userResponse);
            userResponse.setBirthday(convertToString(user.getBirthday()));
            userResponse.setCreatedDate(convertToString(user.getCreatedDate()));
            return userResponse;
        } catch (NoSuchMethodException | IllegalAccessException | InvocationTargetException e) {
            e.printStackTrace();
            return null;
        }
    }
}
