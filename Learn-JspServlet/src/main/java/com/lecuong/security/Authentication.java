package com.lecuong.security;

import com.lecuong.common.ConsantKey;
import com.lecuong.entity.User;
import com.lecuong.listerner.ApplicationListerner;
import com.lecuong.model.response.user.UserResponse;
import com.lecuong.util.SessionUtil;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebFilter("/*")
public class Authentication implements Filter {

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {

    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
            throws IOException, ServletException {

        HttpServletRequest req = (HttpServletRequest) request;
        HttpServletResponse res = (HttpServletResponse) response;

        String url = req.getRequestURI(); //lay ra url, VD: /login, /home

        if (url.contains("/login") || url.equals("/") || url.startsWith("/assets")){
            chain.doFilter(req,response);
            return;
        }

        UserResponse user = ApplicationListerner.userCache.get((String) SessionUtil.get(req, ConsantKey.SESSION_KEY)); //lay ra trong session xem co user nay ko
        if (user == null){
            RequestDispatcher requestDispatcher = req.getRequestDispatcher("/views/login/login.jsp");
            req.setAttribute("message", "Vui lòng đăng nhập để truy cập");
            requestDispatcher.forward(req, response);
        }

        chain.doFilter(req,response);
    }

    @Override
    public void destroy() {

    }
}
