package com.lecuong.service;

import com.lecuong.model.response.role.RoleResponse;

import java.util.List;

public interface RoleService {

    List<RoleResponse> findAllRoleByUserId(long userId);

}
