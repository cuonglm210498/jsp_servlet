package com.lecuong.service.impl;

import com.lecuong.annotation.Autowire;
import com.lecuong.annotation.Service;
import com.lecuong.entity.Role;
import com.lecuong.mapper.RoleMapper;
import com.lecuong.model.response.role.RoleResponse;
import com.lecuong.repository.RoleRepository;
import com.lecuong.service.RoleService;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class RoleServiceImpl implements RoleService {

    @Autowire
    private final RoleRepository roleRepository;

    public RoleServiceImpl(RoleRepository roleRepository) {
        this.roleRepository = roleRepository;
    }


    @Override
    public List<RoleResponse> findAllRoleByUserId(long userId) {

        List<Role> roleList = roleRepository.findRoleById(userId);

        return roleList
                .stream()
                .map(RoleMapper::convertToResponse)
                .collect(Collectors.toList());
    }
}
