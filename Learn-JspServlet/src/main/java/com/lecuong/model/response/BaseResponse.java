package com.lecuong.model.response;

import java.util.List;

public class BaseResponse<T> {

    private long totalItem;
    private List<T> data;

    private BaseResponse(long totalItem, List<T> data) {
        this.totalItem = totalItem;
        this.data = data;
    }

    public static BaseResponse of(long totalItem, List data){
        return new BaseResponse(totalItem, data);
    }

    public long getTotalItem() {
        return totalItem;
    }

    public void setTotalItem(long totalItem) {
        this.totalItem = totalItem;
    }

    public List<T> getData() {
        return data;
    }

    public void setData(List<T> data) {
        this.data = data;
    }
}
