package com.lecuong.cache;

import com.lecuong.annotation.Component;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public class CacheImpl<K, V> implements Cache<K, V> {

    private volatile Map<K, V> cache = new ConcurrentHashMap<>();

    @Override
    public void put(K k, V v) {
        cache.put(k, v);
    }

    @Override
    public V get(K k) {
        return cache.get(k);
    }

    public Map<K, V> getCache(){
        return cache;
    }
}
