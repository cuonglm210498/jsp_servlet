package com.lecuong.repository.impl;

import com.lecuong.annotation.Repository;
import com.lecuong.entity.User;
import com.lecuong.repository.UserRepository;
import static com.lecuong.util.ReflectionUtil.*;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Optional;

@Repository
public class UserRepositoryImpl extends BaseQuery<User, Long> implements UserRepository {

    @Override
    public Optional<User> findUserByUserNameAndPassword(String userName, String password) {
        String sql = "SELECT * FROM user WHERE username = ? AND password = ?";

        try {
            PreparedStatement ps = connection.prepareStatement(sql);
            ps.setObject(1, userName);
            ps.setObject(2, password);

            ResultSet rs = ps.executeQuery();
            User user = null;
            while(rs.next()){
                user = convertToEntity(rs, User.class);
            }

            return Optional.of(user);

        } catch (SQLException | IllegalAccessException | InstantiationException | NoSuchFieldException throwables) {
            throwables.printStackTrace();
            return Optional.empty();
        }
    }
}
