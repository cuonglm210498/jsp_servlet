package com.lecuong.repository;

import com.lecuong.entity.Role;

import java.util.List;

public interface RoleRepository extends JpaRepository<Role, Long> {

    List<Role> findRoleById(long userId);

}
